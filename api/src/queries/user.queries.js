const User = require('../models/User')

exports.saveUser = async (data) => {
  try {
    const user = new User(data)
    return await user.save()
  } catch (error) {
    console.error(error)
  }
}

exports.findUserByEmail = async (email) => {
  try {
    return await User.findOne({
      where: {
        email: email,
      },
    })
  } catch (error) {
    console.error(error)
  }
}
