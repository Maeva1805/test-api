const Category = require('../models/Category')

exports.saveCategory = async (data) => {
  try {
    const category = new Category(data)
    return await category.save()
  } catch (error) {
    console.error(error)
  }
}

exports.findCategoryByName = async (name) => {
  try {
    return await Category.findOne({
      where: {
        name: name,
      },
    })
  } catch (error) {
    console.error(error)
  }
}
