const User = require('./User');
const Category = require('./Category');
const Article = require('./Article');

module.exports = {
    User,
    Category,
    Article
}