const { Model, DataTypes } = require('sequelize')
const connection = require('../config/sequelize')
const User = require('./User')
const Category = require('./Category')

class Article extends Model {}

Article.init(
  {
    title: { type: DataTypes.STRING, allowNull: false },
    content: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: connection,
    modelName: 'Article',
  },
)

Article.belongsTo(User, { as: 'author' })
Article.belongsTo(Category, { as: 'category' })
User.hasMany(Article, { foreignKey: 'userId', as: 'articles' })
Category.hasMany(Article, { foreignKey: 'categoryId', as: 'articlesCatgory' })

// Article.sync({
//   alter: true,
// })

module.exports = Article
