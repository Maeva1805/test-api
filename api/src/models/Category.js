const { Model, DataTypes } = require('sequelize')
const connection = require('../config/sequelize')

class Category extends Model {}
Category.init(
  {
    name: { type: DataTypes.STRING, allowNull: false },
  },
  {
    sequelize: connection,
    modelName: 'Category',
  },
)

// Category.sync({
//   alter: true,
// })

module.exports = Category
