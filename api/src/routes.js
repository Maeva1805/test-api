const express = require('express')

const { login, authJwt } = require('./controller/security.controller')
const {
  getAllUsers,
  createUser,
  editUser,
  deleteUser,
  getUserById,
} = require('./controller/user.controller')
const {
  createArticle,
  getAllArticles,
  editArticle,
  deleteArticle,
  getArticleById,
} = require('./controller/article.controller')

const {
  createCategory,
  getAllCategories,
  editCategory,
  deleteCategory,
  getCategotyById,
} = require('./controller/category.controller')

const router = express.Router()
router.use(express.json())

/**
 * API Auth
 */
router.post('/login', login)
/**
 * API users
 */
router.get('/users', getAllUsers)
router.get('/users/:id', getUserById)
router.post('/users', createUser)
router.put('/users/:id', authJwt, editUser)
router.delete('/users/:id', authJwt, deleteUser)
/**
 * API artciles
 */
router.get('/articles', getAllArticles)
router.get('/articles/:id', getArticleById)
router.post('/articles', authJwt, createArticle)
router.put('/articles/:id', authJwt, editArticle)
router.delete('/articles/:id', authJwt, deleteArticle)

/**
 * API categories
 */
router.post('/categories', authJwt, createCategory)
router.get('/categories', getAllCategories)
router.get('/categories/:id', getCategotyById)
router.put('/categories/:id', authJwt, editCategory)
router.delete('/categories/:id', authJwt, deleteCategory)

module.exports = router
