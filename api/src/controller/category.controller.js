const Category = require('../models/Category')

const Helper = require('../Helper')
const { saveCategory } = require('../queries/category.queries')


const createCategory = async (req, res) => {
  try {
    const category = await Category.create({ ...req.body });
    return res.status(201).json(category);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  } 
};

const getAllCategories = async (req, res) => {
  try {
    const categories = await Category.findAll()
    return res.status(200).json(categories)
  } catch (error) {
    return res.status(400).send(error.message)
  }
}

const editCategory = async (req, res) => {
  try {
    const [nb] = await Category.update(req.body, {
      where: {
        id: req.params.id,
      },
    });
    const category = await Category.findByPk(req.params.id);

    if (nb === 1) return res.status(200).json(category.dataValues);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const deleteCategory = async (req, res) => {
  try {
    const nb = await Category.destroy({
      where: {
        id: req.params.id,
      },
    });

    if (nb) return res.sendStatus(204);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const getCategotyById = async (req, res) => {
  const category = await Category.findByPk(req.params.id)
  if (category) res.json(category)
  else res.sendStatus(404)
}

module.exports = {
  createCategory,
  getAllCategories,
  editCategory,
  deleteCategory,
  getCategotyById,
}
