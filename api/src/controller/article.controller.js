const Article = require("../models/Article");
const Category = require("../models/Category");
const User = require("../models/User");

const createArticle = async (req, res) => {
  const uid = req.decoded.id;
  console.log(req.decoded, req.decoded.id, uid)
  try {
    const article = await Article.create({ ...req.body, userId: uid });
    return res.status(201).json(article);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const getAllArticles = async (req, res) => {
  try {
    const articles = await Article.findAll({
      include: [
        {
          model: User,
          as: "author",
        },
        {
          model: Category,
          as: "category",
        },
      ],
    });
    return res.status(200).json(articles);
  } catch (error) {
    return res.status(400).send(error.message);
  }
};

const editArticle = async (req, res) => {
  try {
    const [nb] = await Article.update(req.body, {
      where: {
        id: req.params.id,
      },
    });
    const article = await Article.findByPk(req.params.id);

    if (nb === 1) return res.status(200).json(article.dataValues);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const deleteArticle = async (req, res) => {
  try {
    const nb = await Article.destroy({
      where: {
        id: req.params.id,
      },
    });

    if (nb) return res.sendStatus(204);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const getArticleById = async (req, res) => {
  const article = await Article.findByPk(req.params.id)
  if (article) res.json(article)
  else res.sendStatus(404)
}

module.exports = {
  createArticle,
  getAllArticles,
  editArticle,
  deleteArticle,
  getArticleById,
}
