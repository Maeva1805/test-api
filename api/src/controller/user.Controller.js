const User = require("../models/User");
const Article = require("../models/Article");

const createUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    return res.status(201).json(user);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const getAllUsers = async (req, res) => {
  try {
    const users = await User.findAll({
      include: [
        {
          model: Article,
          as: "articles",
        },
      ],
    });
    return res.status(200).json(users);
  } catch (error) {
    return res.status(400).send(error.message);
  }
};

const editUser = async (req, res) => {
  try {
    const [nb, data] = await User.update(req.body, {
      where: {
        id: req.params.id,
      },
      returning: true,
    });
    const user = await User.findByPk(req.params.id);

    if (nb === 1) return res.status(200).json(user.dataValues);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const deleteUser = async (req, res) => {
  try {
    const nb = await User.destroy({
      where: {
        id: req.params.id,
      },
    });

    if (nb) return res.sendStatus(204);
    else return res.sendStatus(404);
  } catch (error) {
    return res.status(400).json({ error: error.message });
  }
};

const getUserById = async (req, res) => {
  const user = await User.findByPk(req.params.id)
  if (user) res.json(user)
  else res.sendStatus(404)
}

module.exports = {
  createUser,
  getAllUsers,
  editUser,
  deleteUser,
  getUserById,
}
