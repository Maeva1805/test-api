const references = new Map();

module.exports = {
    getReference: function(ref) {
        return references.get(ref);
    },
    setReference: function(ref, value) {
        references.set(ref, value);
    },
    removeReference: function(ref) {
        references.delete(ref);
    },
    getValue: function(fullPath) {
        const [refKey, ...path] = fullPath.split(".");
        const ref = this.getReference(refKey);
        //prop_access
        const value = path.reduce((acc, keyPath) => acc[keyPath], ref);
        return value;
    }
}
