Feature: Category

    Scenario: I should see all categories
        When I request "GET" "/categories"
        Then I should have an empty array
        And I should have an array with 0 elements

    Scenario: I should create a new category
        Given I have payload
            | name  | test |
        When I request "POST" "/categories" with payload
        Then The response status should be 201
        And I should have an object with the following attributes
            | name | test |
        And I should have the "id" attribute

    @Fixture
    Scenario: I should retrieve the category category1
        When I request "GET" "/categories/{{category1.id}}"
        Then The response status should be 200
        And I should have an object with the following attributes
            | name | Category1 |

    @Fixture
    Scenario: I should not retrieve the category unknown
        When I request "GET" "/categories/-1"
        Then The response status should be 404
        And I should have an empty body