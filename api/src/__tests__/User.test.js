const supertest = require("supertest");
const app = require("../app");
const db = require("../config/sequelize");
const request = supertest(app);
const { User } = require("../models");
const FixtureLoader = require("../features/utils/FixtureLoader");
const fs = require('fs/promises');

let elementId;

const getToken = async () => {
    token = await request
      .post("/login")
      .set("Content-Type", "application/json")
      .send({
        email: "user1@test.fr",
        password: "pwd1",
    });
    return token;
}

beforeAll(async () => {
  db.constructor._cls = new Map();
});

beforeEach(async() => {
  const trx = await db.transaction();
  db.constructor._cls.set('transaction', trx);
})

afterEach(async() => {
  await db.constructor._cls.get('transaction').rollback();
})

afterAll(async() => {
  db.close();
});

describe("User routes", () => {
  it("should create a user", async () => {
    const response = await request
      .post("/users")
      .set("Content-Type", "application/json")
      .send({
        firstName: "Rhoam",
        lastName: "Bosphoramus",
        email: "demo@test.fr",
        password: "test",
      });

    const data = response.body;

    expect(response.status).toBe(201);
    expect(data).toHaveProperty("id");
    elementId = data.id;

    expect(data).toHaveProperty("firstName", "Rhoam");
    expect(data).toHaveProperty("lastName", "Bosphoramus");
    expect(data).toHaveProperty("email", "demo@test.fr");
    // expect(data).toHaveProperty("password", "test");
  });

//   it("should login", async () => {
//     token = await request
//       .post("/login")
//       .set("Content-Type", "application/json")
//       .send({
//         email: "demo@test.fr",
//         password: "test",
//       });
//   });

  it("should return all users", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/user.json")
    );
    const response = await request.get("/users").send();
    const users = await User.findAll();
    expect(response.status).toBe(200);
    const data = response.body
    expect(data).toHaveLength(users.length);
  });

  it("should update a user", async () => {
    await FixtureLoader(
        await fs.realpath(__dirname + "/../features/fixtures/user.json")
      );
      const users = await User.findAll();
      const id = users[0].dataValues.id;

      const token = await getToken();

    const response = await request
      .put(`/users/${id}`)
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        firstName: "Hello",
        lastName: "World",
      });

    const data = response.body;
    console.log("datadebugger", data);

    expect(response.status).toBe(200);
    expect(data).toHaveProperty("firstName", "Hello");
    expect(data).toHaveProperty("lastName", "World");
  });

  it("should delete a user", async () => {
    await FixtureLoader(
        await fs.realpath(__dirname + "/../features/fixtures/user.json")
      );
      const token = await getToken();
      const users = await User.findAll();
      const id = users[0].dataValues.id;

    const response = await request
      .delete(`/users/${id}`)
      .set("Authorization", token.res.rawHeaders[5]);

    expect(response.status).toBe(204);
  });
});
