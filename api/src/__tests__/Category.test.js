const supertest = require("supertest");
const app = require("../app");
const db = require("../config/sequelize");
const request = supertest(app);
const Category = require("../models/Category");

let elementId;
let userId;
let token;

beforeAll(async () => {
  const userResponse = await request
    .post("/users")
    .set("Content-Type", "application/json")
    .send({
      firstName: "Zelda",
      lastName: "Williams",
      email: "zw@test.fr",
      password: "test",
    });
  userId = userResponse.body.id;

  token = await request
    .post("/login")
    .set("Content-Type", "application/json")
    .send({
      email: "zw@test.fr",
      password: "test",
    });
});

afterAll(async () => {
  await request
    .delete(`/users/${userId}`)
    .set("Authorization", token.res.rawHeaders[5]);

  db.close();
});

describe("Categories routes", () => {
  /** --- INIT --- */

  it("should return 0 categories", async () => {
    const response = await request.get("/categories").send();
    expect(response.status).toBe(200);
    const data = response.body;
    expect(data).toHaveLength(0);
  });

  /** --- CREATE --- */

  // OK
  it("should create a category", async () => {
    const response = await request
      .post("/categories")
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        name: "Category test",
      });

    const data = response.body;

    expect(response.status).toBe(201);
    expect(data).toHaveProperty("id");
    elementId = data.id;

    expect(data).toHaveProperty("name", "Category test");
  });

  // unauthorized
  it("should not create => unauthorized", async () => {
    const response = await request
      .post("/categories")
      .set("Content-Type", "application/json")
      .send({
        name: "test not create category",
      });

    const categories = await Category.findAll();

    expect(response.status).toBe(401);
    expect(categories).toHaveLength(1);
  });

  /** --- GET --- */

  it("should return all categories", async () => {
    const response = await request.get("/categories").send();
    const categories = await Category.findAll();

    expect(response.status).toBe(200);
    const data = response.body;
    expect(data).toHaveLength(categories.length);
  });

  /** --- EDIT --- */

  // OK
  it("should update a category", async () => {
    const response = await request
      .put(`/categories/${elementId}`)
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        name: "test update category",
      });

    const data = response.body;

    expect(response.status).toBe(200);
    expect(data).toHaveProperty("name", "test update category");
  });

  // false id
  it("should not update => false id", async () => {
    const response = await request
      .put(`/categories/test`)
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        name: "test not update category",
      });

    expect(response.status).toBe(400);
  });

  /** --- DELETE --- */

  // unauthorized
  it("should not delete => unauthorized", async () => {
    const response = await request.delete(`/categories/${elementId}`);

    expect(response.status).toBe(401);
  });

  // OK
  it("should delete a category", async () => {
    const response = await request
      .delete(`/categories/${elementId}`)
      .set("Authorization", token.res.rawHeaders[5]);

    const categories = await Category.findAll();
    // console.log(categories, elementId)

    expect(response.status).toBe(204);
    expect(categories.length).toBe(0);
  });

  // false id
  it("should not delete => false id", async () => {
    const response = await request
      .delete(`/categories/test`)
      .set("Authorization", token.res.rawHeaders[5]);

    expect(response.status).toBe(400);
  });
});
