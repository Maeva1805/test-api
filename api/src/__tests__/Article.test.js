const supertest = require("supertest");
const app = require("../app");
const db = require("../config/sequelize");
const request = supertest(app);
const { Article, User } = require("../models");
const FixtureLoader = require("../features/utils/FixtureLoader");
const fs = require('fs/promises');

let elementId;
let userId;
let token;

let trxParent;

beforeAll(async () => {
  db.constructor._cls = new Map();

  trxParent = await db.transaction();
  db.constructor._cls.set('transaction', trxParent);

  await FixtureLoader(
    await fs.realpath(__dirname + "/../features/fixtures/user.json")
  );
  const users = await User.findAll();
  // console.log(users)

  token = await request
    .post("/login")
    .set("Content-Type", "application/json")
    .send({
      email: "user1@test.fr",
      password: "pwd1",
    });
});

beforeEach(async() => {
  const trx = await db.transaction();
  db.constructor._cls.set('transaction', trx);
})

afterEach(async() => {
  await db.constructor._cls.get('transaction').rollback();
})

afterAll(async() => {
  db.constructor._cls.set('transaction', trxParent);
  await db.constructor._cls.get('transaction').rollback();
      
  db.close();
});


describe("Articles routes", () => {
  /** --- INIT --- */

  it("should return 0 article", async () => {
    const response = await request.get("/articles").send();
    expect(response.status).toBe(200);
    const data = response.body;
    expect(data).toHaveLength(0);
  });

  /** --- CREATE --- */

  // OK
  it("should create an article", async () => {
    
    const response = await request
      .post("/articles")
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        title: "Article test",
        content: "test content"
      });

    const data = response.body;
    console.log(response.body)

    expect(response.status).toBe(201);
    expect(data).toHaveProperty("id");
    elementId = data.id;

    expect(data).toHaveProperty("title", "Article test");
    expect(data).toHaveProperty("content", "test content");
    // expect(data).toHaveProperty("userId", userId);
  });

  // unauthorized
  it("should not create => unauthorized", async () => {
    const response = await request
      .post("/articles")
      .set("Content-Type", "application/json")
      .send({
        title: "Article test",
        content: "test content"
      });
    
    const articles = await Article.findAll();

    expect(response.status).toBe(401);
    expect(articles).toHaveLength(0);
  });

  /** --- GET --- */

  it("should return all articles", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );
    const response = await request.get("/articles").send();
    const articles = await Article.findAll();

    expect(response.status).toBe(200);
    const data = response.body;
    expect(data).toHaveLength(articles.length);
  });

  /** --- EDIT --- */

  // OK
  it("should update an article", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );
    const articles = await Article.findAll();
    const id = articles[0].dataValues.id;

    const response = await request
      .put(`/articles/${id}`)
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        title: "Edit",
        content: "test edit",
      });

    const data = response.body;

    expect(response.status).toBe(200);
    expect(data).toHaveProperty("title", "Edit");
    expect(data).toHaveProperty("content", "test edit");
  });

  // false id
  it("should not update => false id", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );
    const response = await request
      .put(`/articles/test`)
      .set("Content-Type", "application/json")
      .set("Authorization", token.res.rawHeaders[5])
      .send({
        title: "Edit",
        content: "test edit",
      });

    expect(response.status).toBe(400);
  });

  /** --- DELETE --- */

  // unauthorized
  it("should not delete => unauthorized", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );
    const articles = await Article.findAll();
    const id = articles[0].dataValues.id;

    const response = await request
      .delete(`/articles/${id}`)

    expect(response.status).toBe(401);
  });

  // OK
  it("should delete an article", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );
    const articles = await Article.findAll();
    const id = articles[0].dataValues.id;

    const response = await request
      .delete(`/articles/${id}`)
      .set("Authorization", token.res.rawHeaders[5]);

    const updatedList = await Article.findAll();

    expect(response.status).toBe(204);
    expect(updatedList.length).toBe(1);
  });

  // false id
  it("should not delete => false id", async () => {
    await FixtureLoader(
      await fs.realpath(__dirname + "/../features/fixtures/article.json")
    );

    const response = await request
      .delete(`/articles/test`)
      .set("Authorization", token.res.rawHeaders[5]);

    expect(response.status).toBe(400);
  });
});
