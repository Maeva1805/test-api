const connection = require('./config/sequelize')

connection.sync({ alter: true }).then(() => {
  console.log('Database synced')
  connection.close()
})
